import React, { useContext } from 'react';

import projectContext from '../../../../domain/context/ProjectContext';
import ListPokemons from '../ListPokemons';
import NotFoundPokemon from '../NotFoundPokemon/NotFoundPokemon';
import Spinner from '../SpinnerComponent/Spinner';
import useGrid from './GridHook';

const PokemonGrid = () => {
  const { local, filter } = useContext(projectContext);
  const [listPokemons, loading] = useGrid();

  if (loading) return <Spinner />;

  return listPokemons.length === 0 && filter !== '' ? (
    <div>
      <NotFoundPokemon local={local} />
    </div>
  ) : (
    <div className="mt-5 w-full md:w-full grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-5 lg:gap-7">
      <ListPokemons pokemons={listPokemons} />
    </div>
  );
};

export default PokemonGrid;
