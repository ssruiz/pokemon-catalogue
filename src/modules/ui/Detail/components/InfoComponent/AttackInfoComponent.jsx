import React from 'react';
import PropTypes from 'prop-types';
import InfoComponent from './InfoComponent';

const AttackInfoComponent = ({ attacks }) => {
  if (attacks === undefined) return null;
  return (
    <div className="divide-y-2 border-2 w-full h-full divide-blue-200">
      <p className="py-2 md:text-lg font-bold">Attacks: </p>
      {attacks.map((at) => {
        const { name, damage, text } = at;
        return (
          <div className="w-full p-2 divide-dashed" key={name}>
            <InfoComponent title="Name:" description={name} />
            <InfoComponent title="Damage:" description={damage} />
            <InfoComponent title="About:" description={text} />
          </div>
        );
      })}
    </div>
  );
};

AttackInfoComponent.propTypes = {
  attacks: PropTypes.arrayOf(PropTypes.any).isRequired,
};
export default AttackInfoComponent;
