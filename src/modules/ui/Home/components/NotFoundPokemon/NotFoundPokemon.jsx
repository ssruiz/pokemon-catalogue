import React from 'react';
import PropTypes from 'prop-types';

import NotFoundImg from '../../../../../assets/img/not_found.png';

const NotFoundPokemon = ({ local }) => (
  <div className="mt-10 flex flex-col w-full items-center">
    <img
      className="w-44 md:w-80 lg:w-96"
      src={NotFoundImg}
      alt="Not found Pokemon"
    />
    {local ? (
      <p className="p-4 text-center mt-2 md:text-xl text-gray-900">
        No pokemon found with that name in the current list :( Press enter to search in
        database
      </p>
    ) : (
      <p className="p-4 text-center mt-2 md:text-xl text-gray-900">
        No pokemon found with that name in the database :(
      </p>
    )}
    <a
      className="mt-2 text-sm text-blue-900 "
      href="https://www.deviantart.com/tea-tiger/art/psyduck-265177082  "
      target="_blank"
      rel="noopener noreferrer"
    >
      Art by: tea-tiger
    </a>
  </div>
);

NotFoundPokemon.propTypes = {
  local: PropTypes.bool.isRequired,
};
export default NotFoundPokemon;
