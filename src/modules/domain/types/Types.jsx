export const GET_POKEMONS = 'GET_POKEMONS';
export const SET_POKEMONS = 'SET_POKEMONS';
export const GET_DETAIL = 'GET_DETAIL';
export const SET_POKEMON_FILTER = 'SET_POKEMON_FILTER';
export const SET_LOCAL_SEARCH = 'SET_LOCAL_SEARCH';
export const SET_PER_PAGE = 'SET_PER_PAGES';
export const SET_PAGES = 'SET_PAGES';
export const SET_PAGE = 'SET_PAGE';
