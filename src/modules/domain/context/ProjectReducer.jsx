import {
  GET_POKEMONS,
  SET_LOCAL_SEARCH,
  SET_PAGE,
  SET_PAGES,
  SET_PER_PAGE,
  SET_POKEMONS,
  SET_POKEMON_FILTER,
} from '../types/Types';

export default (state, action) => {
  switch (action.type) {
    case GET_POKEMONS:
      return {
        ...state,
        pokemons: action.payload,
      };
    case SET_POKEMON_FILTER:
      return {
        ...state,
        filter: action.payload,
      };
    case SET_POKEMONS:
      return {
        ...state,
        pokemons: action.payload,
      };
    case SET_LOCAL_SEARCH:
      return {
        ...state,
        local: action.payload,
      };
    case SET_PAGES:
      return {
        ...state,
        pages: action.payload,
      };
    case SET_PAGE:
      return {
        ...state,
        page: action.payload,
      };
    case SET_PER_PAGE:
      return {
        ...state,
        perPage: action.payload,
      };
    default:
      return state;
  }
};
