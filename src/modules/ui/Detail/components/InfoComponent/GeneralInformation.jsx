import React from 'react';
import PropTypes from 'prop-types';
import InfoComponent from './InfoComponent';

const GeneralInfoComponent = ({
  name,
  rarity,
  series,
  supertype,
  subtype,
  nationalPokedexNumber,
  hp,
}) => (
  <div className="w-full md:h-full divide-y-2 divide-gray-400">
    <div className="p-2">
      <p className=" md:text-lg font-bold text-center">General Info</p>
    </div>
    <div className="p-2">
      <InfoComponent title="Name:" description={name} />
      {nationalPokedexNumber && (
        <InfoComponent
          title="Pokedex Number:"
          description={nationalPokedexNumber.toString()}
        />
      )}

      <InfoComponent title="Rarity:" description={rarity} />
      <InfoComponent title="Series:" description={series} />
      <InfoComponent title="HP:" description={hp} />
      <InfoComponent title="Super-type:" description={supertype} />
      <InfoComponent title="Sub-type:" description={subtype} />
    </div>
  </div>
);

GeneralInfoComponent.propTypes = {
  name: PropTypes.string.isRequired,
  rarity: PropTypes.string.isRequired,
  series: PropTypes.string.isRequired,
  supertype: PropTypes.string.isRequired,
  subtype: PropTypes.string.isRequired,
  nationalPokedexNumber: PropTypes.number.isRequired,
  hp: PropTypes.string.isRequired,
};
export default GeneralInfoComponent;
