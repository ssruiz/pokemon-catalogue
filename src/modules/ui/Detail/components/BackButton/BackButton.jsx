import React from 'react';
import { Link } from 'react-router-dom';

const BackButton = () => (
  <Link to="/home">
    <div>
      <button
        className="border-0 border-none p-3 md:p-4 bg-red-500 hover:bg-red-600 transition duration-500 focus:bg-green-500 text-white md:text-lg rounded-full"
        type="button"
      >
        <i className="fa fa-home mr-2" />
        Back home
      </button>
    </div>
  </Link>
);

export default BackButton;
