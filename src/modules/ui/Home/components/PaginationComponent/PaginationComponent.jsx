import React, { useContext } from 'react';
import toTheTop from '../../../../../core/utils/ToTheTop';
import projectContext from '../../../../domain/context/ProjectContext';

const PaginationComponent = () => {
  const {
    page,
    setPage,
    perPage,
    pages,
  } = useContext(projectContext);

  const handlePreviousOnclick = () => {
    if (perPage === 0 || page - 1 < 0) return;
    setPage(page - 1);
    toTheTop();
  };

  const handleNextOnclick = () => {
    if (perPage === 0 || page + 1 === pages) return;
    setPage(page + 1);
    toTheTop();
  };
  return (
    <div className="ml-2 p-2 w-2/6 flex text-sm justify-end items-center">
      <div className="border w-10 md:w-auto p-2 rounded-md border-red-500 md:py-4 mr-4 md:px-10  md:mx-10">
        {page + 1}
        /
        {pages === 0 ? 1 : pages}
      </div>
      <div className="flex w-28">
        <button
          type="button"
          className=" text-sm rounded-md bg-red-500 text-white w-full hover:bg-red-600 focus:bg-red-700 p-1 mr-1  transition-colors duration-500"
          onClick={handlePreviousOnclick}
        >
          <span className="text-2xl mr-2">&laquo;</span>
        </button>
        <button
          type="button"
          className="rounded-md bg-red-500 text-white align-middle p-1 w-full hover:bg-red-600 focus:bg-red-700  transition-colors duration-500"
          onClick={handleNextOnclick}
        >
          <span className="text-2xl ml-2">&raquo;</span>
        </button>
      </div>
    </div>
  );
};

export default PaginationComponent;
