import { getListPokemons, getPokemonByName } from '../../data/PokemonService';
import {
  GET_POKEMONS,
  SET_LOCAL_SEARCH,
  SET_PAGE,
  SET_PAGES,
  SET_PER_PAGE,
  SET_POKEMONS,
  SET_POKEMON_FILTER,
} from '../types/Types';

// Call the pokemonService's function getListPokemons and updates the state
export const getAllPokemons = async (dispatch) => {
  console.log('asd');
  const list = await getListPokemons();
  dispatch({
    type: GET_POKEMONS,
    payload: list,
  });

  dispatch({
    type: SET_PAGES,
    payload: 0,
  });

  dispatch({
    type: SET_PER_PAGE,
    payload: 0,
  });

  dispatch({
    type: SET_PAGE,
    payload: 0,
  });
};

// Update the value of filter in state
export const filterPokemons = (dispatch, value) => {
  dispatch({
    type: SET_POKEMON_FILTER,
    payload: value,
  });
};

// Call the pokemonService's function getPokemonByName and updates the state
export const setPokemonsByName = async (dispatch, name) => {
  const pokemons = await getPokemonByName(name);
  dispatch({
    type: SET_POKEMONS,
    payload: pokemons,
  });
};

// Update the value of local in state
export const setLocal = (dispatch, value) => {
  dispatch({
    type: SET_LOCAL_SEARCH,
    payload: value,
  });
};

// Set the total of pages of the pokemon list
export const setTotalPages = (dispatch, value) => {
  console.log(value);
  dispatch({
    type: SET_PAGES,
    payload: value,
  });
};

// Set the current page
export const setCurrentPage = (dispatch, value) => {
  dispatch({
    type: SET_PAGE,
    payload: value,
  });
};

export const setPokemonPerPage = (dispatch, value) => {
  dispatch({
    type: SET_PER_PAGE,
    payload: value,
  });
};
