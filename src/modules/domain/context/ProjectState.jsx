import React, { useReducer } from 'react';
import PropTypes from 'prop-types';

import projectContext from './ProjectContext';
import projectReducer from './ProjectReducer';

import {
  filterPokemons,
  getAllPokemons,
  setLocal,
  setPokemonsByName,
  setTotalPages,
  setCurrentPage,
  setPokemonPerPage,
} from './Actions';

import { getDetailPokemon } from '../../data/PokemonService';

const ProjectState = ({ children }) => {
  const initialState = {
    pokemons: [],
    filter: '',
    local: true,
    pages: 0,
    perPage: 0,
    page: 0,
  };

  // Dispathc to execute actions
  const [state, dispatch] = useReducer(projectReducer, initialState);

  // actions

  const getPokemons = () => {
    setLocal(dispatch, true);
    getAllPokemons(dispatch);
  };

  const setPokemons = () => {
    setLocal(dispatch, false);
    setPokemonsByName(dispatch, state.filter);
  };

  const setFilter = (value) => {
    filterPokemons(dispatch, value);
  };

  const getDetail = (id) => getDetailPokemon(id);

  const setPages = (value) => {
    setTotalPages(dispatch, value);
  };

  const setPage = (value) => {
    setCurrentPage(dispatch, value);
  };

  const setPerPage = (value) => {
    const valueInt = parseInt(value, 10);
    if (valueInt === 0) setPages(0);
    else setPages(Math.ceil(state.pokemons.length / valueInt));

    setPokemonPerPage(dispatch, valueInt);
  };

  return (
    <projectContext.Provider
      value={{
        pokemons: state.pokemons,
        filter: state.filter,
        local: state.local,
        pages: state.pages,
        page: state.page,
        perPage: state.perPage,
        getPokemons,
        setPokemons,
        setFilter,
        getDetail,
        setPage,
        setPerPage,
      }}
    >
      {children}
    </projectContext.Provider>
  );
};

// Props validation
ProjectState.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ProjectState;
