import { useContext, useEffect, useState } from 'react';
import toTheTop from '../../../../../core/utils/ToTheTop';
import ProjectContext from '../../../../domain/context/ProjectContext';

// Hook to manage the state in the Grid of Pokemons
const useGrid = () => {
  const projectState = useContext(ProjectContext);
  const {
    pokemons,
    getPokemons,
    filter,
    pages,
    perPage,
    page,
  } = projectState;

  const [listPokemons, setlistPokemons] = useState([]);
  const [currentPage, setCurrentPage] = useState(0);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getPokemons();
  }, []);

  useEffect(() => {
    setlistPokemons(pokemons);
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 1500);
  }, [pokemons]);

  useEffect(() => {
    if (pages === 0) setlistPokemons(pokemons);
    else {
      const shift = pokemons.slice(0, perPage);
      setlistPokemons(shift);
    }
    toTheTop();
  }, [perPage]);

  useEffect(() => {
    if (page === 0 && perPage === 0) setlistPokemons(pokemons);
    else if (page > currentPage) {
      const shift = page + perPage - 1;
      const newList = pokemons.slice(shift, shift + perPage);
      setlistPokemons(newList);
    } else {
      const shift = page;
      const newList = pokemons.slice(shift, shift + perPage);
      setlistPokemons(newList);
    }
    setCurrentPage(page);
  }, [page]);

  useEffect(() => {
    if (filter.length === 0) {
      setlistPokemons(pokemons);
      return;
    }
    setlistPokemons(
      listPokemons.filter((pokemon) => pokemon.name.toLowerCase().includes(filter.toLowerCase())),
    );
  }, [filter]);

  return [
    listPokemons,
    loading,
  ];
};

export default useGrid;
