import React, { Fragment } from 'react';

import { useParams } from 'react-router-dom';

import HeaderComponent from '../Home/components/HeaderComponent/HeaderComponent';
import BackButton from './components/BackButton/BackButton';
import PokemonDetail from './components/PokemonDetail/PokemonDetail';

const PokemonDetailPage = () => {
  const { id } = useParams();

  return (
    <>
      <div className="sticky w-full">
        <HeaderComponent />
      </div>
      <div className="flex flex-col content-between container mx-auto p-4 h-full mt-4">
        <BackButton />
        <PokemonDetail id={id} />
      </div>
    </>
  );
};

export default PokemonDetailPage;
