import axios from 'axios';
import {
  API_URL,
  API_URL_GET_CARDS,
  API_URL_GET_DETAIL,
} from '../../core/config/constans';

// Funtcion that gets the list of pokemons from the API
export const getListPokemons = async () => {
  const result = await axios.get(`${API_URL}${API_URL_GET_CARDS}`);
  return result.data.cards;
};
export const getDetailPokemon = async (id) => {
  const result = await axios.get(`${API_URL}${API_URL_GET_DETAIL}${id}`);
  console.log(result);
  return result.data.card;
};

export const getPokemonByName = async (name) => {
  const result = await axios.get(`${API_URL}${API_URL_GET_CARDS}&name=${name}`);
  return result.data.cards;
};
