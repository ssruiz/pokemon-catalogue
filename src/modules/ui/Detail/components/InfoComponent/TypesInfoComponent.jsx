import React from 'react';
import PropTypes from 'prop-types';

const TypesInfoComponent = ({ types }) => {
  if (types === undefined) return null;
  return (
    <div className="w-full md:h-full divide-y-2 divide-gray-400">
      <div className="p-2">
        <p className=" md:text-lg font-bold text-center">Types</p>
      </div>
      <div className="p-2">
        {types.map((t) => (
          <span key={t}>{t}</span>
        ))}
      </div>
    </div>
  );
};

TypesInfoComponent.propTypes = {
  types: PropTypes.arrayOf(PropTypes.string).isRequired,
};
export default TypesInfoComponent;
