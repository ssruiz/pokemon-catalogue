import React, { useContext } from 'react';
import projectContext from '../../../../domain/context/ProjectContext';

const SearchFormComponent = () => {
  const {
    setFilter,
    setPokemons,
    getPokemons,
    setPerPage,
    local,
  } = useContext(
    projectContext,
  );

  const filterList = (e) => {
    setFilter(e.target.value);
    if (e.target.value === '' && !local) getPokemons();
    setPerPage(0);
  };

  const searchPokemon = async (e) => {
    e.preventDefault();
    setPokemons();
  };

  return (
    <form
      onSubmit={searchPokemon}
      className="relative w-2/6 border-b-2 focus-within:border-red-500"
    >
      <input
        className="block w-full appearance-none focus:outline-none bg-white my-2"
        type="text"
        id="search"
        name="search"
        placeholder=" "
        onChange={filterList}
      />
      <label
        className="text-sm md:text-lg absolute top-2 text-gray-600 origin-0 duration-300"
        htmlFor="search"
      >
        Search by name
      </label>
    </form>
  );
};

export default SearchFormComponent;
