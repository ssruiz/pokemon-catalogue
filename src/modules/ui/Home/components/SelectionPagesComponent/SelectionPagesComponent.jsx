import React, { useContext } from 'react';
import projectContext from '../../../../domain/context/ProjectContext';

const SelectionPagesComponent = () => {
  const { setPerPage, setPage, perPage } = useContext(projectContext);

  const changePages = (e) => {
    setPerPage(e.target.value);
    setPage(0);
  };

  return (
    <div className="flex w-2/6 flex-wrap flex-grow justify-center space-x-5 items-center p-2">
      <label className="hidden md:inline-block" htmlFor="pages">
        Items
      </label>
      <select
        className="text-sm w-14 mr-2 rounded-md md:flex-grow bg-red-500 text-white p-2  md:w-5/12 hover:bg-red-600 transition-colors duration-500"
        name="pages"
        id="pages"
        onChange={changePages}
        value={perPage}
      >
        <option value="0">All</option>
        <option value="5">5</option>
        <option value="10">10</option>
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="100">100</option>
      </select>
    </div>
  );
};

export default SelectionPagesComponent;
