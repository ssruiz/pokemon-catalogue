import React from 'react';
import PropTypes from 'prop-types';
import './index.css';
import { Link } from 'react-router-dom';

const PokemonCard = ({ pokemon }) => {
  const { name, imageUrl, id } = pokemon;
  return (
    <Link to={`/detail/${id}`}>
      <div className="pokecard flex flex-col w-auto py-8 px-4 md:px-5 lg:px-5 items-center border-2 shadow-xl border-gray-100 rounded-xl divide-solid divide-y-2 divide-gray-500 bg-white hover:bg-red-500 transition duration-500 ease-in-out">
        <p className="font-sans text-base subpixel-antialiased md:text-lg lg:text-xl font-semibold mb-2">
          {name}
        </p>

        <div className="">
          <img className="object-cover mt-2" src={imageUrl} alt={name} />
        </div>
      </div>
    </Link>
  );
};

PokemonCard.propTypes = {
  pokemon: PropTypes.objectOf(PropTypes.any).isRequired,
};
export default PokemonCard;
