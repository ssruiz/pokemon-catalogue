import React from 'react';

import PaginationComponent from '../PaginationComponent/PaginationComponent';
import SearchFormComponent from '../SearchFormComponent/SearchFormComponent';
import SelectionPagesComponent from '../SelectionPagesComponent/SelectionPagesComponent';
import './index.css';

const FilterComponent = () => (
  <div className="w-full fixed border-2 bg-white p-4 ">
    <div className="flex flex-row justify-start md:justify-evenly  items-center  ">
      <SearchFormComponent />
      <SelectionPagesComponent />
      <PaginationComponent />
    </div>
  </div>
);

export default FilterComponent;
