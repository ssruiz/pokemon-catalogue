// function that push to the top of the page
const toTheTop = () => {
  window.scrollTo(0, 0);
};

export default toTheTop;
