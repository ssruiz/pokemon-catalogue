import React from 'react';

import ProjectState from './modules/domain/context/ProjectState';

import Navigator from './core/config/Navigator';

const App = () => (
  <ProjectState>
    <Navigator />
  </ProjectState>
);
export default App;
