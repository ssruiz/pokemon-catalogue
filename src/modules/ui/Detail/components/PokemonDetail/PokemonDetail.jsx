import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import projectContext from '../../../../domain/context/ProjectContext';
import Spinner from '../../../Home/components/SpinnerComponent/Spinner';
import TypesInfoComponent from '../InfoComponent/TypesInfoComponent';
import AttackInfoComponent from '../InfoComponent/AttackInfoComponent';
import WeaknessComponent from '../InfoComponent/WeaknessComponent';
import GeneralInfoComponent from '../InfoComponent/GeneralInformation';

const PokemonDetail = ({ id }) => {
  const { getDetail } = useContext(projectContext);

  const [pokemonDetail, setPokemonDetail] = useState({});

  useEffect(async () => {
    const detail = await getDetail(id);
    console.log(detail);
    setPokemonDetail(detail);
  }, []);

  if (Object.keys(pokemonDetail).length === 0) return <Spinner />;

  const {
    name,
    hp,
    subtype,
    supertype,
    imageUrlHiRes,
    nationalPokedexNumber,
    rarity,
    series,
    types,
    attacks,
    weaknesses,
  } = pokemonDetail;

  return (
    <div className="mt-4 md:h-full flex flex-col items-center md:justify-start sm:space-y-2 md:items-center md:flex-row md:space-x-4">
      <img
        className=" w-96 object-scale-down md:h-96 md:w-auto"
        src={imageUrlHiRes}
        alt={name}
      />
      <div className="flex flex-col items-center md:space-y-1  bg-white border-2 shadow-md w-full ml-2 rounded-lg divide-y-2 divide-gray-400 divide-x-4">
        <p className="inline-block md:text-2xl font-bold py-8">
          Here&apos;s your pokemon!
        </p>
        <div className="grid grid-cols-1 w-full gap-4 md:gap-0 md:grid-cols-2 lg:grid-cols-4 grid-flow-row text-center divide-y-4 md:divide-x-4">
          <GeneralInfoComponent
            name={name}
            subtype={subtype}
            supertype={supertype}
            nationalPokedexNumber={nationalPokedexNumber}
            rarity={rarity}
            series={series}
            hp={hp}
          />
          {types && <TypesInfoComponent types={types} />}
          {attacks && <AttackInfoComponent attacks={attacks} />}
          {weaknesses && <WeaknessComponent weaknesses={weaknesses} />}
        </div>
      </div>
    </div>
  );
};

PokemonDetail.propTypes = {
  id: PropTypes.string.isRequired,
};

export default PokemonDetail;
