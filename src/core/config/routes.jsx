import PokemonDetailPage from '../../modules/ui/Detail/PokemonDetailPage';
import HomePage from '../../modules/ui/Home/HomePage';

const routes = [
  {
    path: '/home',
    component: HomePage,
  },
  {
    path: '/detail/:id',
    component: PokemonDetailPage,
  },
  {
    path: '/*',
    component: HomePage,
  },
];

export default routes;
