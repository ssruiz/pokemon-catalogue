import React from 'react';
import PropTypes from 'prop-types';
import InfoComponent from './InfoComponent';

const WeaknessComponent = ({ weaknesses }) => {
  if (weaknesses === undefined) return null;
  return (
    <div className="divide-y-2 h-full divide-gray-400 w-full divide-blue-200">
      <p className="py-2 md:text-lg font-bold">Weaknesses: </p>
      {weaknesses.map((weakness) => {
        const { type, value } = weakness;
        return (
          <div className="w-full p-2" key={type}>
            <InfoComponent title="Type:" description={type} />
            <InfoComponent title="Value:" description={value} />
          </div>
        );
      })}
    </div>
  );
};

WeaknessComponent.propTypes = {
  weaknesses: PropTypes.arrayOf(PropTypes.any).isRequired,
};
export default WeaknessComponent;
