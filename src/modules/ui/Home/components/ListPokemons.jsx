import React from 'react';
import PropTypes from 'prop-types';

import PokemonCard from './PokemonCard/PokemonCard';

const ListPokemons = ({ pokemons }) => {
  if (pokemons.length === 0) return null;
  return (
    <>
      {pokemons.map((p) => (
        <PokemonCard key={p.id} pokemon={p} />
      ))}
    </>
  );
};

ListPokemons.propTypes = {
  pokemons: PropTypes.arrayOf(PropTypes.any).isRequired,
};

export default ListPokemons;
