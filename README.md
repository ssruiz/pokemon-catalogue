<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="src/assets/img/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Pokemon Catalogue</h3>

  <p align="center">
    A Willdom's Code Challenge
    <br />
    <a href="https://pokemoncataloguewilldom.netlify.app/">View Demo</a>
  </p>
</p>
<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

[![Product Name Screen Shot][product-screenshot]](src/assets/img/logo.png)

Project part of the Willdom's Code Challenge.

A pokemons catalogue where you cand find info about them. The app allows the visualization of pokemons, following the following instructions:

- In the catalog, show only the image and name of the Pokemon.
- Add Pokemons filter. It should be searchable by name.
- It must allows to navigate to the detail of any Pokemon that appears in the list
- In the Pokemon detail, show some relevant information available.
- It should be possible to return to the catalog from the detail of any Pokemon

Made in react using parcel as bundler.

Note:
   last stable parcel's version seems to have troubles using with  postcss 8, tailwindcss use this, so degrades had to be made on this libraries' versions


### Built With

- [React](https://reactjs.org/)
- [ParcelJs](https://parceljs.org/)
- [Tailwindcss](https://tailwindcss.com/)

<!-- GETTING STARTED -->

## Getting Started

### Prerequisites

- yarn or npm

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/ssruiz/pokemon-catalogue
   ```
2. Install yarn/NPM packages
   ```sh
   yarn / npm install
   ```

<!-- USAGE EXAMPLES -->

## Usage

to run locally

```sh
   yarn / npm start
```

Building

```sh
   yarn / npm build
```

## Contact

Samuel Sebastian Ruiz Ibarra - ssruiz6@gmail.com

Project Link: [https://gitlab.com/ssruiz/pokemon-catalogue](https://gitlab.com/ssruiz/pokemon-catalogue)

<!-- ACKNOWLEDGEMENTS -->

## Acknowledgements

- [Tobias Ahlin - CSS Spinners](https://tobiasahlin.com/spinkit/)
- [Tea-tiger for the psyduck art ](https://www.deviantart.com/tea-tiger/art/psyduck-265177082)
- [TailwindCSS](https://tailwindcss.com/)
- [PostCSS](https://postcss.org/)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[product-screenshot]: src/assets/img/screenshot.png

```

```
