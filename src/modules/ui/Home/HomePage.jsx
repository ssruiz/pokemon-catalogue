import React from 'react';
import HeaderComponent from './components/HeaderComponent/HeaderComponent';

import PokemonGrid from './components/PokemonGrid/PokemonGrid';
import FilterComponent from './components/FilterComponent/FilterComponent';

const HomePage = () => (
  <div className="flex flex-col">
    <div className="fixed w-full">
      <HeaderComponent />
      <FilterComponent />
    </div>
    <div className="container mx-auto mt-52 md:mt-72 ">
      <PokemonGrid />
    </div>
  </div>
);
export default HomePage;
